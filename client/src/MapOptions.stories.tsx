import { ComponentStory, ComponentMeta } from "@storybook/react";

import MapOptions from "./MapOptions";

export default {
  title: "ESCG Map/Map Options Panel",
  component: MapOptions,
  argTypes: {
    primaryColor: { control: "color" },
    secondaryColor: { control: "color" },
    textColor: { control: "color" },
  },
} as ComponentMeta<typeof MapOptions>;

const Template: ComponentStory<typeof MapOptions> = (args) => (
  <MapOptions {...args} />
);

export const DefaultMapOptions = Template.bind({});
DefaultMapOptions.args = {
  panelWidth: 300,
  primaryColor: "#016BA7",
  secondaryColor: "#A0D9F9",
  textColor: "#000000",
};
