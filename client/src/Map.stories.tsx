import { ComponentStory, ComponentMeta } from "@storybook/react";

import Map from "./Map";

export default {
  title: "ESCG Map/Map",
  component: Map,
  argTypes: {
    primaryColor: { control: "color" },
    secondaryColor: { control: "color" },
    textColor: { control: "color" },
  },
} as ComponentMeta<typeof Map>;

const Template: ComponentStory<typeof Map> = (args) => <Map {...args} />;

export const DefaultMap = Template.bind({});
DefaultMap.args = {
  maxRadius: 10000,
  panelWidth: 300,
  primaryColor: "#016BA7",
  secondaryColor: "#A0D9F9",
  textColor: "#000000",
};
