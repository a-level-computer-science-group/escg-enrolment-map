const serverAddress = "https://140.238.102.168:8080";
const timeoutDuration = 300;

export enum Campus {
  Eastbourne = "Eastbourne",
  Hastings = "Hastings",
  Lewes = "Lewes",
  Newhaven = "Newhaven",
}

export enum Sex {
  Male = "M",
  Female = "F",
}

export interface Population {
  filtered: number;
  unfiltered: number;
}

export interface CampusPopulation {
  eastbourne: Population;
  hastings: Population;
  lewes: Population;
  newhaven: Population;
}

export interface APIResponse {
  eastbourne: number;
  hastings: number;
  lewes: number;
  newhaven: number;
}

export interface Filter {
  filter_year?: string;
  filter_sex?: Sex;
  filter_campus?: Campus;
  filter_area_code?: string;
  filter_qualification_type?: string;
  filter_school?: string;
}

export interface OptionData {
  year?: string[];
  qualification_type?: string[];
  sex?: string[];
  campus?: string[];
  school?: string[];
  areacode?: string[];
}

export type Coordinate = [number, number];
export interface CoordinateData {
  eastbourne: Coordinate,
  hastings: Coordinate,
  lewes: Coordinate,
  newhaven: Coordinate,
}

export function getColor(campus: string) {
  if (campus === "eastbourne") return "#DEA400";
  if (campus === "lewes") return "#D60058";
  if (campus === "hastings") return "#029442";
  return "#016BA7";
}

async function getRequest(body: string, subdomain: string) {
  try {
    const controller = new AbortController();
    const requestOptions: RequestInit = {
      signal: controller.signal,
      method: "POST",
      body,
    };
    const timeoutId = setTimeout(() => controller.abort(), timeoutDuration);
    const response = await fetch(serverAddress + subdomain, requestOptions);
    clearTimeout(timeoutId);
    const json = await response.json();
    return json;
  } catch (err) {
    return null;
  }
}

export async function getCoordinateData() {
  console.log("Sending coordinate data request.");
  const json = await getRequest("", "/coordinates");
  console.log("Coordinate Data: \n", json);
  return json as CoordinateData;
}

export async function getOptionData() {
  console.log("Sending options data request...");
  const json = await getRequest("", "/options")
  console.log("Options Data:", json);
  return json as OptionData;
}

export async function getData(filters: Filter) {
  console.log("Sending student count request...");
  const json = await getRequest(JSON.stringify(filters), "/count");
  console.log("Student count response: ", json);
  return json as APIResponse;
}
