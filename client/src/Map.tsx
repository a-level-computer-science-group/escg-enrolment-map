import React, { useCallback, useEffect, useState, useRef } from "react";
import { Circle, MapContainer, Marker, Popup, TileLayer } from "react-leaflet";

import {
  getData,
  Filter,
  APIResponse,
  getColor,
  Population,
  getCoordinateData,
  CoordinateData,
  getOptionData,
  OptionData,
  Coordinate,
} from "./data";
import "./Map.scss";
import MapOptions from "./MapOptions";

interface MapProps {
  maxRadius?: number;

  // for MapOptions panel
  panelWidth?: number;
  primaryColor?: string;
  secondaryColor?: string;
  textColor?: string;
}

function Map({
  maxRadius: defaultMaxRadius = 10000,

  // for MapOptions panel
  panelWidth = 300,
  primaryColor = "#016BA7",
  secondaryColor = "#A0D9F9",
  textColor = "#000000",
}: MapProps) {
  // section is edited only by initial useEffect
  const [coordinates, setCoordinates] = useState({} as CoordinateData);
  const [optionData, setOptionData] = useState({} as OptionData);

  const [maxRadius, setMaxRadius] = useState(defaultMaxRadius);
  const [filters, setFilters] = useState({} as Filter);
  const [masterFilters, setMasterFilters] = useState({} as Filter);
  const [campusData, setCampusData] = useState({} as APIResponse);
  const [masterCampusData, setMasterCampusData] = useState({} as APIResponse);
  const [apiResponsive, setApiResponsive] = useState(true);
  const timeoutRef = useRef(null as NodeJS.Timeout | null);

  const mapOptionsConfig = {
    panelWidth,
    primaryColor,
    secondaryColor,
    textColor,
    hidePanel: { ...campusData } === ({} as APIResponse),
  };

  const setInitialData = useCallback(async (): Promise<void> => {
    const initialData = getData({});
    const getCoords = getCoordinateData();
    const getOptions = getOptionData();
    Promise.all([initialData, getCoords, getOptions])
      .then(
        ([initData, coords, options]: [
          APIResponse,
          CoordinateData,
          OptionData
        ]) => {
          if (!(initData && coords && options)) setApiResponsive(false);
          else {
            setApiResponsive(true);
            if (timeoutRef.current) clearTimeout(timeoutRef.current);
            setCoordinates(coords);
            setCampusData(initData || {});
            setMasterCampusData(initData || {});
            setOptionData(options);
          }
        }
      )
      .catch(() => {
        setApiResponsive(false);
      });
  }, []);

  // set initial data
  useEffect(() => {
    setInitialData();
  }, [setInitialData]);

  const queueInit = useCallback(() => {
    timeoutRef.current = setTimeout(() => {
      if (!apiResponsive) {
        console.log("Checking API...");
        setInitialData();
        queueInit();
      }
    }, 10000);
  }, [apiResponsive, setInitialData]);

  // on apiResponsive change
  useEffect(() => {
    if (!apiResponsive) queueInit();
  }, [apiResponsive, queueInit]);

  // update filters on change
  useEffect(() => {
    console.log("=== filters were changed ===");
    try {
      const newData = getData(filters);
      newData.then((data) => setCampusData(data));
    } catch (err) {}
  }, [filters]);

  // update master filters on checkbox change
  useEffect(() => {
    console.log("=== master filters were changed ===");
    try {
      const newData = getData(masterFilters);
      newData.then((data) => setMasterCampusData(data));
    } catch (err) {}
  }, [masterFilters]);

  // TODO improve this
  function getPopupText(pop: Population, campus: string): string {
    const currentFilters = { ...filters }; // copy, not reference
    const filterEntries = Object.entries(currentFilters);
    if (filterEntries.length === 0)
      return `${pop.unfiltered} students at ${
        campus.substr(0, 1).toUpperCase() + campus.substr(1)
      } campus.`;
    const preValues: string[] = Object.entries(currentFilters)
      .map(([key, value]) => {
        if (key === "filter_sex") {
          if (value === "M") return "male";
          if (value === "F") return "female";
        }
        if (key === "filter_qualification_type") {
          return value;
        }
        return "";
      })
      .filter((value) => !!value);
    const postValues: string[] = Object.entries(currentFilters)
      .map(([key, value]) => {
        if (key === "filter_year") {
          return "attended for years " + value;
        }
        if (key === "filter_area_code") {
          return "had areacode " + value;
        }
        if (key === "filter_school") {
          return "came from " + value;
        }
        return "";
      })
      .filter((value) => !!value);
    const campusString = currentFilters.filter_campus
      ? " also attend " + currentFilters.filter_campus + " campus"
      : "";

    if (postValues.length > 1)
      postValues[postValues.length - 1] =
        " and " + postValues[postValues.length - 1];
    if (postValues.length > 0) postValues[0] = "who " + postValues[0];

    const popString =
      pop.filtered === 0
        ? "No"
        : pop.filtered < 5
        ? "Less than 5"
        : pop.filtered;
    const preString = preValues.join(" ");
    const postString = postValues.join(" ");

    return `${popString}${preString ? " " + preString : ""} students at ${
      campus.substr(0, 1).toUpperCase() + campus.substr(1)
    } campus${campusString}${postString ? " " + postString : ""}.`;
  }

  const calcRadius = (() => {
    const totals = Object.keys(masterCampusData || {}).map(
      (k) => masterCampusData[k as keyof CoordinateData]
    );
    const largestTotal = totals.reduce(
      (tot, val) => (val > tot ? val : tot),
      0
    );
    const modifier = maxRadius / largestTotal;
    return function (count: number): number {
      return count * modifier;
    };
  })();

  function CampusCircles() {
    if (!(coordinates && coordinates["newhaven"])) return UseBasicMarkers(); // stop if the response was empty
    const campusCircles = [];
    for (const key in campusData) {
      const radius = {
        filtered:
          calcRadius(campusData && campusData[key as keyof CoordinateData]) ||
          0,
        unfiltered:
          calcRadius(
            masterCampusData && masterCampusData[key as keyof CoordinateData]
          ) || 0,
      };
      campusCircles.push(
        ...(["filtered", "unfiltered"] as const).map((str, idx, arr) => {
          let popup = false;
          if (str === "unfiltered") popup = true;
          return getCircleFromString(str, key as keyof CoordinateData, {
            radius: radius[str],
            popup,
          });
        })
      );
    }
    console.log("CampusCircles: \n", campusCircles);
    return campusCircles;

    function UseBasicMarkers() {
      return ([
        { campus: "Eastbourne", coords: [50.78829, 0.27139] },
        { campus: "Lewes", coords: [50.87031, 0.01546] },
        { campus: "Hastings", coords: [50.85811, 0.57799] },
        { campus: "Newhaven", coords: [50.7964, 0.04926] },
      ] as { campus: string; coords: Coordinate }[]).map((data) => {
        return (
          <Marker position={data.coords}>
            <Popup>{`${data.campus} campus.`}</Popup>
          </Marker>
        );
      });
    }

    function getCircleFromString(
      str: string,
      key: keyof CoordinateData,
      { radius = 0, popup = false }
    ) {
      const params = {
        center: coordinates[key],
        radius: radius,
        pathOptions:
          str === "filtered"
            ? {
                fillColor: getColor(key),
                weight: 0,
              }
            : {
                color: getColor(key as keyof CoordinateData),
                fillOpacity: 0,
              },
        key: `${key}:${str}`,
      };
      if (!popup) return <Circle {...params} />;
      return (
        <Circle {...params}>
          <Popup
            closeButton={true}
            autoClose={false}
            closeOnClick={false}
            closeOnEscapeKey={false}
          >
            {getPopupText(
              {
                filtered: campusData && campusData[key],
                unfiltered: masterCampusData && masterCampusData[key],
              },
              key
            )}
          </Popup>
        </Circle>
      );
    }
  }

  function ApiUnresponsiveMessage() {
    return (
      <div className="warning-wrapper">
        <h3 className="warning-message">No reponse from student data API.</h3>
      </div>
    );
  }

  return (
    <div id="map-wrapper">
      {apiResponsive ? undefined : ApiUnresponsiveMessage()}
      <MapContainer center={[50.78829, 0.271392]} zoom={11} id="map">
        <TileLayer
          url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='An <a href="https://www.escg.ac.uk/">ESCG</a> student data map by Layton Burchell and Ethan Brierley | Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>' // removed text: ' contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
          subdomains={["a", "b", "c"]}
        />
        {CampusCircles()}
        {coordinates && coordinates["newhaven"] ? (
          <Marker position={coordinates["newhaven"]}>
            <Popup>
              Newhaven campus is a new addition to the East Sussex College
              Group.
              <br />
              Expect more soon.
            </Popup>
          </Marker>
        ) : undefined}
      </MapContainer>
      <MapOptions
        {...{
          maxRadius,
          setMaxRadius,
          optionData,
          setFilters,
          setMasterFilters,
          ...mapOptionsConfig,
        }}
      />
    </div>
  );
}

export default Map;
