import React, {
  ChangeEvent,
  useState,
  useEffect,
  SetStateAction,
  Dispatch,
  useCallback,
} from "react";

import { Filter, OptionData } from "./data";
import "./MapOptions.scss";

interface MapOptionsProps {
  panelWidth?: number;
  primaryColor?: string;
  secondaryColor?: string;
  textColor?: string;
  hidePanel?: boolean;
  optionData?: OptionData;
  setFilters: (filters: Filter) => void;
  setMasterFilters: (filters: Filter) => void;
  maxRadius: number;
  setMaxRadius: (radius: number) => void;
}

// by extending PureComponent, this object only runs componentDidUpdate
// when it detects changes in state on a shallow difference check
function MapOptions({
  panelWidth = 300,
  primaryColor = "#016BA7",
  secondaryColor = "#A0D9F9",
  textColor = "#000000",
  hidePanel = true,
  optionData = {},
  setFilters = (filters: Filter) => {},
  setMasterFilters = (filters: Filter) => {},
  maxRadius = 0,
  setMaxRadius = (radius: number) => {},
}: MapOptionsProps) {
  const panelHideTransitionDuration = 0.4;

  const [yearValue, setYearValue] = useState("");
  const [yearMaster, setYearMaster] = useState(false);
  const [qualificationValue, setQualificationValue] = useState("");
  const [qualificationMaster, setQualificationMaster] = useState(false);
  const [sexValue, setSexValue] = useState("");
  const [sexMaster, setSexMaster] = useState(false);
  const [campusValue, setCampusValue] = useState("");
  const [campusMaster, setCampusMaster] = useState(false);
  const [schoolValue, setSchoolValue] = useState("");
  const [schoolMaster, setSchoolMaster] = useState(false);
  const [areacodeValue, setAreacodeValue] = useState("");
  const [areacodeMaster, setAreacodeMaster] = useState(false);
  const [expanded, setExpanded] = useState(!hidePanel);

  // run on first load only
  useEffect(() => {
    const style = document.documentElement.style;

    style.setProperty("--panel-width", panelWidth + "px");
    style.setProperty("--primary-color", primaryColor);
    style.setProperty("--secondary-color", secondaryColor);
    style.setProperty("--text-color", textColor);
    style.setProperty(
      "--menu-transition-time",
      panelHideTransitionDuration + "s"
    );

    const optionsWrapper: HTMLElement | null = document.querySelector(
      ".map-options-wrapper"
    );
    if (optionsWrapper)
      setTimeout(() => {
        optionsWrapper.style.transition =
          "all var(--menu-transition-time) ease-in-out";
        optionsWrapper.style.display = "block";
      }, 50);
  }, [panelWidth, primaryColor, secondaryColor, textColor]);

  const getFilters = useCallback(
    (onlyMasters?: boolean) => {
      const filters = ([
        { filter_year: yearValue },
        { filter_sex: sexValue },
        { filter_campus: campusValue },
        { filter_school: schoolValue },
        {
          filter_qualification_type: qualificationValue,
        },
        {
          filter_area_code: areacodeValue.toUpperCase(),
        },
      ] as Filter[])
        // filter away non-master values if onlyMasters is true
        .filter((val) => {
          if (!onlyMasters) return true;
          switch (Object.keys(val)[0] as keyof Filter) {
            case "filter_year":
              return yearMaster;
            case "filter_sex":
              return sexMaster;
            case "filter_campus":
              return campusMaster;
            case "filter_school":
              return schoolMaster;
            case "filter_qualification_type":
              return qualificationMaster;
            case "filter_area_code":
              return areacodeMaster;
            default:
              return false;
          }
        })
        .reduce((obj, val) => {
          if (Object.keys(val).some((k) => val[k as keyof Filter]))
            obj = { ...obj, ...val };
          return obj;
        }, {}) as Filter;
      return filters;
    },
    [
      yearValue,
      yearMaster,
      sexValue,
      sexMaster,
      campusValue,
      campusMaster,
      schoolValue,
      schoolMaster,
      qualificationValue,
      qualificationMaster,
      areacodeValue,
      areacodeMaster,
    ]
  );

  // set filters on filter change
  useEffect(() => {
    const filters = getFilters();
    setFilters(filters);
  }, [
    getFilters,
    setFilters,
    yearValue,
    sexValue,
    campusValue,
    schoolValue,
    qualificationValue,
    areacodeValue,
  ]);

  // set master filters on filter or master change
  useEffect(() => {
    const filters = getFilters(true);
    setMasterFilters(filters);
  }, [
    getFilters,
    setMasterFilters,
    yearValue,
    yearMaster,
    sexValue,
    sexMaster,
    campusValue,
    campusMaster,
    schoolValue,
    schoolMaster,
    qualificationValue,
    qualificationMaster,
    areacodeValue,
    areacodeMaster,
  ]);

  // FIXME display: none // display: block // NOT hidden
  /** Toggle whether the Map Options panel is expanded. */
  function onExpandButtonClick() {
    setExpanded((e) => !e);
    const options: HTMLElement | null = document.querySelector(".map-options");
    if (!options) return;
    if (!expanded) options.style.display = "block";
    else
      setTimeout(
        () => (options.style.display = "none"),
        panelHideTransitionDuration * 1000
      );
  }

  // Toggle options panel on API response
  useEffect(() => {
    const expandButton: HTMLElement | null = document.querySelector(
      ".expand-button"
    );
    if (!expandButton) return;
    expandButton.click();
  }, [hidePanel]);

  /** Performs extra actions before updating filters. */
  function onSelectChange(
    e: ChangeEvent<HTMLSelectElement>,
    setValue: Dispatch<SetStateAction<string>>
  ): void {
    setValue(e.currentTarget.value);
  }

  /** Introduces a 500ms delay between the end of typing and filter change. */
  const onInputChange = (() => {
    let timeoutId: NodeJS.Timeout; // keep timout in cache
    return (
      e: ChangeEvent<HTMLInputElement>,
      setValue: Dispatch<SetStateAction<string>>
    ): void => {
      timeoutId && clearTimeout(timeoutId); // clear if timeout exists
      timeoutId = setTimeout(() => setValue(e.target.value), 500);
    };
  })();

  // was being used to make only 1 checkbox tickable at a time
  // const onCheckboxInput = (() => {
  //   let lastChecked: HTMLInputElement | undefined;
  //   return (e: ChangeEvent<HTMLInputElement>): void => {
  //     if (lastChecked?.checked) lastChecked.checked = false;
  //     lastChecked = e.target;
  //   };
  // })();

  function capitalizeEach(str: string) {
    return str
      .split(/[_ ]/i)
      .map((w: string) => capitalizeFirst(w))
      .join(" ");
  }

  function capitalizeFirst(str: string) {
    return str.substr(0, 1).toUpperCase() + str.substr(1);
  }

  function checkboxFromString(str: string) {
    const setValue = getSetterFromKey(str as keyof OptionData);
    return (
      <input
        type="checkbox"
        id={"master-" + str}
        name={"master-" + str}
        className="master-checkbox"
        onInput={
          setValue
            ? (e: ChangeEvent<HTMLInputElement>) => setValue(e.target.checked)
            : undefined
        }
      />
    );

    function getSetterFromKey(key: keyof OptionData) {
      switch (key) {
        case "year":
          return setYearMaster;
        case "qualification_type":
          return setQualificationMaster;
        case "sex":
          return setSexMaster;
        case "campus":
          return setCampusMaster;
        case "school":
          return setSchoolMaster;
        case "areacode":
          return setAreacodeMaster;
        default:
          return null;
      }
    }
  }

  function selectFromData(filterName: keyof OptionData) {
    const props = getSelectDetails(filterName);
    const options = optionsFromData(filterName);
    // TODO reduce this
    if (!props?.asSelect || !options?.[0])
      return (
        <div className="input-wrapper">
          <input
            type="text"
            name={"input-" + filterName}
            id={"input-" + filterName}
            className="filter-input"
            placeholder={props?.placeholder || capitalizeFirst(filterName)}
            onChange={(e) =>
              onInputChange(
                e,
                (props as {
                  setValue: React.Dispatch<React.SetStateAction<string>>;
                }).setValue
              )
            }
          />
          {checkboxFromString(filterName)}
        </div>
      );

    return (
      <div className="input-wrapper">
        <select
          name={props.id}
          id={props.id}
          className={(props.value ? "" : "greyed") + " filter-input"}
          value={props.value}
          onChange={(e) =>
            onSelectChange(
              e,
              (props as {
                setValue: React.Dispatch<React.SetStateAction<string>>;
              }).setValue
            )
          }
        >
          {options}
        </select>
        {checkboxFromString(filterName)}
      </div>
    );

    function optionsFromData(
      filterName: keyof OptionData
    ): JSX.Element[] | null {
      const elements = optionData?.[filterName]?.map((o: string) => {
        return (
          <option value={o} key={o}>
            {capitalizeOption(o)}
          </option>
        );
      });
      elements?.unshift(
        <option value="" key="placeholder">
          {capitalizeEach(filterName)}
        </option>
      );
      return elements || null;
    }

    function capitalizeOption(str: string): string {
      const doNotCap = ["for", "and"];
      const alwaysCap = ["utc"];
      const words = str.split(/(?<=[ ()@])|(?=@)/gi);
      return words
        .map((w, i) => {
          const t = w.trim();
          if (alwaysCap.includes(t)) {
            const letters = [];
            for (const l of w) {
              letters.push(l.toUpperCase());
            }
            return letters.join("");
          } else if (i === 0 || !doNotCap.includes(t))
            return capitalizeFirst(w);
          return w;
        })
        .join("");
    }

    function getSelectDetails(
      filterName: keyof OptionData
    ): {
      asSelect: boolean;
      id: string;
      placeholder: string;
      value: string;
      setValue: React.Dispatch<React.SetStateAction<string>>;
    } | null {
      switch (filterName) {
        case "year":
          return {
            asSelect: true,
            id: "filter-year",
            placeholder: "Year",
            value: yearValue,
            setValue: setYearValue,
          };
        case "qualification_type":
          return {
            asSelect: true,
            id: "filter-qualification-type",
            placeholder: "Qualification Type",
            value: qualificationValue,
            setValue: setQualificationValue,
          };
        case "sex":
          return {
            asSelect: true,
            id: "filter-sex",
            placeholder: "Sex",
            value: sexValue,
            setValue: setSexValue,
          };
        case "campus":
          return {
            asSelect: true,
            id: "filter-campus",
            placeholder: "Campus",
            value: campusValue,
            setValue: setCampusValue,
          };
        case "school":
          return {
            asSelect: true,
            id: "filter-school",
            placeholder: "School",
            value: schoolValue,
            setValue: setSchoolValue,
          };
        case "areacode":
          return {
            asSelect: false,
            id: "filter-areacode",
            placeholder: "Area code (eg, BN22)",
            value: areacodeValue,
            setValue: setAreacodeValue,
          };
        default:
          return null;
      }
    }
  }

  function resetFilters(e: any) {
    const allElements = Array.prototype.flatMap.call(
      document.getElementsByClassName(
        "input-wrapper"
      ) as HTMLCollectionOf<Element>,
      (el: HTMLElement) => [...el.childNodes]
    ) as HTMLElement[];
    const selectElements = allElements.filter(
      (el: HTMLElement) => el.nodeName === "SELECT"
    ) as HTMLSelectElement[];
    const inputElements = allElements.filter(
      (el: HTMLElement) => el.nodeName === "INPUT"
    ) as HTMLInputElement[];
    const textInputs = inputElements.filter(
      (el: HTMLInputElement) => el.id.search("master") === -1
    );
    const checkboxInputs = inputElements.filter(
      (el: HTMLInputElement) => el.id.search("master") !== -1
    );

    for (let el of selectElements) el.selectedIndex = 0;
    for (let el of textInputs) el.value = "";
    for (let el of checkboxInputs) el.checked = false;

    setFilters({});
    setMasterFilters({});
  }

  return (
    <div
      className="map-options-wrapper"
      style={{
        right: expanded ? "0" : "calc(var(--panel-width) * -1)",
        maxWidth: expanded ? "100%" : "",
      }}
    >
      <button className="expand-button" onClick={onExpandButtonClick}>
        ≡
      </button>
      <div
        className="map-options"
        style={{
          pointerEvents: expanded ? "auto" : "none",
        }}
      >
        <div className="filters">
          <div className="filters-titles">
            <h3 className="filters-title">Filters</h3>
          </div>
          {([
            "year",
            "qualification_type",
            "sex",
            "campus",
            "school",
            "areacode",
          ] as const).map((d) => selectFromData(d))}
          <div className="slider-section">
            <h4 className="slider-title">Circle Size</h4>
            <div className="slider-wrapper">
              <input
                type="range"
                name="size-slider"
                id="size-slider"
                className="slider"
                min="1000"
                max="30000"
                step="1000"
                defaultValue="10000"
                contentEditable="true"
                onChange={(e) => setMaxRadius(parseInt(e.target.value))}
              />
              <div id="slider-number">{maxRadius / 1000 + " km"}</div>
            </div>
          </div>
          <button id="reset-button" onClick={(e) => resetFilters(e)}>
            Reset Filters
          </button>
        </div>
      </div>
    </div>
  );
}

export default MapOptions;
