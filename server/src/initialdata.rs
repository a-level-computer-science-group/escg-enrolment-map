use crate::data::{Campus, QualificationType, School, Sex, Year};
use indexmap::IndexSet;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Coordinates {
  eastbourne: [f32; 2],
  lewes: [f32; 2],
  hastings: [f32; 2],
  newhaven: [f32; 2],
}
impl Coordinates {
  pub fn new() -> Self {
    Coordinates {
      eastbourne: Campus::Eastbourne.coords(),
      hastings: Campus::Hastings.coords(),
      lewes: Campus::Lewes.coords(),
      newhaven: Campus::Newhaven.coords(),
    }
  }
}

#[derive(Serialize, Deserialize)]
pub struct Options {
  year: Vec<Year>,
  qualification_type: Vec<QualificationType>,
  sex: Vec<Sex>,
  campus: Vec<Campus>,
  school: Vec<School>,
}
impl Options {
  pub fn get_options() -> Self {
    let student_data = (*crate::STUDENT_DATA).clone();

    let mut idx_year = IndexSet::<Year>::new();
    let mut idx_qualification_type = IndexSet::<QualificationType>::new();
    let mut idx_sex = IndexSet::<Sex>::new();
    let mut idx_campus = IndexSet::<Campus>::new();
    let mut idx_school = IndexSet::<School>::new();
    for student in student_data {
      idx_year.insert(student.year);
      idx_qualification_type.insert(student.qualification_type);
      idx_sex.insert(student.sex);
      idx_campus.insert(student.campus);
      if student.school.0.find("�") == None {
        let school_out: School = School { 0: student.school.0.to_lowercase() };
        idx_school.insert(school_out);
      }
    };
    idx_year.sort();
    idx_qualification_type.sort();
    idx_sex.sort();
    idx_campus.sort();
    idx_school.sort();

    let year = idx_year.into_iter().collect();
    let qualification_type = idx_qualification_type.into_iter().collect();
    let sex = idx_sex.into_iter().collect();
    let campus = idx_campus.into_iter().collect();
    let school = idx_school.into_iter().collect();

    Self {
      year,
      qualification_type,
      sex,
      campus,
      school,
    }
  }
}
