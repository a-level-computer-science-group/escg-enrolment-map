use serde::{Deserialize, Deserializer, Serialize};
use std::{
  fs::File,
  io::{self, Read},
};

#[derive(Deserialize, PartialEq, Eq, PartialOrd, Ord, Serialize, Clone, Hash, Debug)]
pub struct Year(String);

#[derive(Deserialize, PartialEq, Eq, PartialOrd, Ord, Serialize, Clone, Hash, Debug)]
pub enum Sex {
  #[serde(rename = "M")]
  Male,
  #[serde(rename = "F")]
  Female,
}

#[derive(Deserialize, PartialEq, Eq, PartialOrd, Ord, Serialize, Clone, Hash, Debug)]
pub enum Campus {
  Eastbourne,
  Hastings,
  Lewes,
  Newhaven,
  #[serde(rename = "N/K")]
  Unknown,
}

impl Campus {
  pub fn coords(self) -> [f32; 2] {
    match self {
      Campus::Eastbourne => [50.78829, 0.27139],
      Campus::Lewes => [50.87031, 0.01546],
      Campus::Hastings => [50.85811, 0.57799],
      Campus::Newhaven => [50.79640, 0.04926],
      Campus::Unknown => panic!(),
    }
  }
}

#[derive(Deserialize, PartialEq, Eq, PartialOrd, Ord, Serialize, Clone, Hash, Debug)]
pub struct QualificationType(String);

#[derive(Deserialize, PartialEq, Eq, PartialOrd, Ord, Serialize, Clone, Hash, Debug)]
pub struct School(pub String);

#[derive(Deserialize, PartialEq, Eq, PartialOrd, Ord, Serialize, Clone, Debug)]
pub struct AreaCode(String);

fn parse_area_code<'de, D>(deserializer: D) -> Result<AreaCode, D::Error>
where
D: Deserializer<'de>,
{
  Ok(AreaCode(
    String::deserialize(deserializer)?.trim().to_uppercase().to_owned(),
  ))
}

#[derive(Deserialize, PartialEq, Serialize, Clone, Debug)]
pub struct Student {
  #[serde(rename = "FTEnrolmentID")]
  enrolment_id: u32,
  #[serde(rename = "Year")]
  pub year: Year,
  #[serde(rename = "Sex")]
  pub sex: Sex,
  #[serde(rename = "Campus")]
  pub campus: Campus,
  #[serde(rename = "PostCodePart1")]
  #[serde(deserialize_with = "parse_area_code")]
  pub area_code: AreaCode,
  #[serde(rename = "QualificationType")]
  pub qualification_type: QualificationType,
  #[serde(rename = "School")]
  pub school: School,
}

pub fn open_student_file() -> io::Result<String> {
  let mut file = File::open("StudentData_03_03_2020.json")?;
  let mut contents = String::new();
  file.read_to_string(&mut contents)?;
  Ok(contents)
}
