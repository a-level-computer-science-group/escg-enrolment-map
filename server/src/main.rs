mod data;
mod query;
mod initialdata;

use actix_cors::Cors;
use actix_web::{App, HttpResponse, HttpServer, Responder, post};
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};
use data::{open_student_file, Campus, Student};
use initialdata::{Coordinates, Options};
use query::Query;

use once_cell::sync::Lazy;

type StudentData = Vec<Student>;
pub static STUDENT_DATA: Lazy<StudentData> =
Lazy::new(|| serde_json::from_str(&open_student_file().unwrap()).unwrap());
pub static TOTAL_COUNT: Lazy<Count> =
Lazy::new(|| Count::count_student_campus((*STUDENT_DATA).clone()));

#[post("/coordinates")]
async fn coordinates() -> impl Responder {
  // dbg!(&req_body);
  let coords: Coordinates = Coordinates::new();
  HttpResponse::Ok().json(coords)
}
#[post("/options")]
async fn options() -> impl Responder {
  // dbg!(&req_body);
  let options: Options = Options::get_options();
  HttpResponse::Ok().json(options)
}
#[post("/count")]
async fn count(req_body: String) -> impl Responder {
  // dbg!(&req_body);
  let query: Query = serde_json::from_str(&req_body).unwrap();
  HttpResponse::Ok().json(query.run_query())
}

// #[actix_web::main]
// async fn main() -> std::io::Result<()> {
//   dbg!(Count::count_student_campus((*STUDENT_DATA).clone()));
//   HttpServer::new(|| {
//     let cors = Cors::default()
//       .allow_any_origin() // possible GDPR problem
//       .max_age(3600);
//     App::new()
//       .wrap(cors)
//       .service(count)
//       .service(options)
//       .service(coordinates)
//   })
//   // can be bound to multiple addresses
//   .bind("127.0.0.1:8080")?
//   .run()
//   .await
// }
#[actix_web::main]
async fn main() -> std::io::Result<()> {
  // load TLS keys
  // to create a self-signed temporary cert for testing:
  // `openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'`
  let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
  builder
      .set_private_key_file("key.pem", SslFiletype::PEM)
      .unwrap();
  builder.set_certificate_chain_file("cert.pem").unwrap();

  // HttpServer::new(|| App::new().service(index))

  dbg!(Count::count_student_campus((*STUDENT_DATA).clone()));
  HttpServer::new(|| {
    let cors = Cors::default()
      .allow_any_origin() // possible GDPR problem
      .max_age(3600);
    App::new()
      .wrap(cors)
      .service(count)
      .service(options)
      .service(coordinates)
  })
      .bind_openssl("127.0.0.1:8080", builder)?
      .run()
      .await
}

#[derive(Debug, Clone, Copy)]
pub struct Count {
  eastbourne: u32,
  hastings: u32,
  lewes: u32,
  newhaven: u32,
}

impl Count {
  pub fn count_student_campus(student_data: StudentData) -> Self {
    let mut eastbourne = 0;
    let mut hastings = 0;
    let mut lewes = 0;
    let mut newhaven = 0;
    for student in student_data {
      match student.campus {
        Campus::Eastbourne => eastbourne += 1,
        Campus::Hastings => hastings += 1,
        Campus::Lewes => lewes += 1,
        Campus::Newhaven => newhaven += 1,
        Campus::Unknown => {}
      }
    }
    Self {
      eastbourne,
      lewes,
      hastings,
      newhaven,
    }
  }
}
