use crate::data::{AreaCode, Campus, QualificationType, School, Sex, Year};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Debug)]
pub struct Query {
  filter_year: Option<Year>,
  filter_sex: Option<Sex>,
  filter_campus: Option<Campus>,
  filter_area_code: Option<AreaCode>,
  filter_qualification_type: Option<QualificationType>,
  filter_school: Option<School>,
}
impl Query {
  pub fn run_query(self) -> Response {
    let mut student_data = (*crate::STUDENT_DATA).clone();

    if let Query {
      filter_year: Some(ref year),
      ..
    } = self
    {
      student_data = student_data
      .into_iter()
      .filter(|student| &student.year == year)
      .collect();
    }

    if let Query {
      filter_sex: Some(ref sex),
      ..
    } = self
    {
      student_data = student_data
      .into_iter()
      .filter(|student| &student.sex == sex)
      .collect();
    }

    if let Query {
      filter_campus: Some(ref campus),
      ..
    } = self
    {
      student_data = student_data
      .into_iter()
      .filter(|student| &student.campus == campus)
      .collect();
    }

    if let Query {
      filter_area_code: Some(ref area_code),
      ..
    } = self
    {
      student_data = student_data
      .into_iter()
      .filter(|student| &student.area_code == area_code)
      .collect();
    }

    if let Query {
      filter_qualification_type: Some(ref qualification_type),
      ..
    } = self
    {
      student_data = student_data
      .into_iter()
      .filter(|student| &student.qualification_type == qualification_type)
      .collect();
    }

    if let Query {
      filter_school: Some(mut school),
      ..
    } = self
    {
      if school.0.find("�") != None {
        school.0 = school.0.replace("�", "'")
      }
      student_data = student_data
      .into_iter()
      .filter(|student| student.school.0.to_lowercase().replace("�", "'") == school.0.to_lowercase())
      .collect();
    }

    let crate::Count {
      eastbourne,
      hastings,
      lewes,
      newhaven,
    } = crate::Count::count_student_campus(student_data);
    // dbg!((eastbourne, hastings, lewes));
    Response::new(eastbourne, hastings, lewes, newhaven)
  }
}

#[derive(Serialize)]
struct Ratio {
  filtered: u32,
  unfiltered: u32,
}

#[derive(Serialize)]
pub struct Response {
  eastbourne: u32,
  hastings: u32,
  lewes: u32,
  newhaven: u32,
}

impl Response {
  fn new(eastbourne: u32, hastings: u32, lewes: u32, newhaven: u32) -> Self {
    Response {
      eastbourne,
      hastings,
      lewes,
      newhaven,
    }
  }
}
